import express from 'express';
import {getWeather} from "../controllers/darksky";

const router = express.Router();

router.get('/getWeather/:latitude/:longitude', getWeather);

export default router;



