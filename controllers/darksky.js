import endpoints from '../conf/endpoints';
import fetch from 'node-fetch';
import redis from 'redis';
import Coordinates from 'capitals-coordinates';

export const getWeather = async (req, res) => {

    const client = redis.createClient(process.env.REDIS_URL);

    client.on('connect', () => {
        console.log('Redis está ejecutándose!');
    });

    client.on('error', (err) => {
        res.status(500).send('Problema con el servidor. Inténtelo más tarde.' + err);
    });

    const longitude = req.params.longitude;
    const latitude = req.params.latitude;

    const capital = await getCapitalCoords(latitude, longitude);

    if (capital.error === undefined) {

        client.exists(capital.country, async (err, reply) => {

            if (err) {
                res.status(500).send('Problemas con el servidor. Inténlo más tarde.');
            }

            if (reply) {
                console.log('Existe en Redis');

                client.get(capital.country, (err, reply) => {
                    if (err) {
                        res.status(500).send('Problemas con el servidor. Inténtelo más tarde.');
                    }

                    res.status(200).send(JSON.parse(reply.toString()));
                });


            } else {
                const url = `${endpoints.DarkskyUrl}${capital.capitalLatitude},${capital.capitalLongitude}?lang=es&units=auto&exclude=hourly,flags,offset`;

                let error = Math.random().toFixed(1);

                while (error !== '0.1') {
                    console.log(`${error} ERROR`);

                    error = Math.random().toFixed(1);
                }

                console.log(`${error} OK`);

                try {
                    const l = await fetch(url,
                        {
                            method: 'GET'
                        });

                    if (l.ok) {
                        const weather = await l.json();

                        const response = {
                            country: capital.country,
                            countryCapital: capital.capitalName,
                            summary: weather.currently.summary,
                            icon: weather.currently.icon,
                            temperature: weather.currently.temperature
                        };

                        client.set(capital.country, JSON.stringify(response), 'EX', '3600', redis.print);

                        client.quit();

                        res.status(200).send(response);
                    } else {
                        client.quit();

                        res.status(500).send({weather: 'error'});
                    }
                } catch (e) {
                    client.quit();

                    res.status(500).send({weather: `error: ${e}`});
                }
            }
        });
    } else {
        res.status(500).send('Problemas al buscar país.');
    }
};

const getCapitalCoords = async (lat, lng) => {
    try {
        const url = `${endpoints.GeoCodeApi}${lat},${lng}${endpoints.GeoCodeExtra}`;

        console.log(url);

        const response = await fetch(url,
            {
                method: 'GET'
            });

        if (response.ok) {
            const jsonResponse = await response.json();

            const results = jsonResponse.results;

            const resultsLength = results.length;

            const country = results[resultsLength - 1]['formatted_address'];

            const capital = await getGeoJson(country);

            return {
                country,
                capitalName: capital[0]['properties']['capital'],
                capitalLatitude: capital[0]['geometry']['coordinates'][1],
                capitalLongitude: capital[0]['geometry']['coordinates'][0]
            };
        }
    } catch (e) {
        return {error: e};
    }

};

const getGeoJson = (country) => {
    const data = Coordinates.rawData;

    return data.filter((item) => {
        return item.properties.country === country;
    });
};