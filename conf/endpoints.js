import {key, googleKey} from '../conf/key';

const DarkskyUrl = `https://api.darksky.net/forecast/${key}/`;

const GeoCodeApi = `https://maps.googleapis.com/maps/api/geocode/json?latlng=`;
const GeoCodeExtra = `&sensor=false&key=${googleKey}&language=en`;

export default {
    DarkskyUrl,
    GeoCodeApi,
    GeoCodeExtra
};